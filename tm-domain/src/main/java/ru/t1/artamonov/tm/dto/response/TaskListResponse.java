package ru.t1.artamonov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.dto.model.TaskDTO;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TaskListResponse extends AbstractResponse {

    @Nullable
    private List<TaskDTO> tasks;

    public TaskListResponse(@Nullable final List<TaskDTO> tasks) {
        this.tasks = tasks;
    }

}
