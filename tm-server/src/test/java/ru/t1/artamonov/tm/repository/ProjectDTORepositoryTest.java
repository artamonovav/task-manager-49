package ru.t1.artamonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.artamonov.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.artamonov.tm.api.repository.dto.IUserDTORepository;
import ru.t1.artamonov.tm.api.service.IConnectionService;
import ru.t1.artamonov.tm.api.service.IPropertyService;
import ru.t1.artamonov.tm.dto.model.ProjectDTO;
import ru.t1.artamonov.tm.marker.UnitCategory;
import ru.t1.artamonov.tm.repository.dto.ProjectDTORepository;
import ru.t1.artamonov.tm.repository.dto.UserDTORepository;
import ru.t1.artamonov.tm.service.ConnectionService;
import ru.t1.artamonov.tm.service.PropertyService;

import javax.persistence.EntityManager;

import static ru.t1.artamonov.tm.constant.ProjectTestData.*;
import static ru.t1.artamonov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectDTORepositoryTest {

    @Nullable
    private static EntityManager entityManager;

    @Nullable
    private static IUserDTORepository userRepository;

    @Nullable
    private static IProjectDTORepository projectRepository;

    @BeforeClass
    public static void init() {
        @NotNull IPropertyService propertyService = new PropertyService();
        @NotNull IConnectionService connectionService = new ConnectionService(propertyService);
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDTORepository(entityManager);
        projectRepository = new ProjectDTORepository(entityManager);
    }

    @AfterClass
    public static void connectionClose() {
        userRepository.remove(USER1);
        entityManager.close();
    }

    @Before
    public void transactionStart() {
        entityManager.getTransaction().begin();
        if (userRepository.findOneById(USER1.getId()) == null) userRepository.add(USER1);
        if (userRepository.findOneById(USER2.getId()) == null) userRepository.add(USER2);
        if (userRepository.findOneById(ADMIN.getId()) == null) userRepository.add(ADMIN);
    }

    @After
    public void transactionEnd() {
        entityManager.getTransaction().rollback();
    }

    @Test
    public void add() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT1);
        projectRepository.findOneById(USER1_PROJECT1.getId());
        Assert.assertEquals(USER1_PROJECT1.getId(), projectRepository.findAll().get(0).getId());
    }

    @Test
    public void clear() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        for (@NotNull ProjectDTO project : USER1_PROJECT_LIST) {
            projectRepository.add(project);
        }
        Assert.assertEquals(USER1_PROJECT_LIST.size(), projectRepository.findAll().size());
        projectRepository.clear(USER2.getId());
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        projectRepository.clear(USER1.getId());
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER2_PROJECT1);
        projectRepository.clear(USER1.getId());
        Assert.assertFalse(projectRepository.findAll().isEmpty());
    }

    @Test
    public void clearAll() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        for (@NotNull ProjectDTO project : USER1_PROJECT_LIST) {
            projectRepository.add(project);
        }
        Assert.assertEquals(USER1_PROJECT_LIST.size(), projectRepository.findAll().size());
        projectRepository.clearAll();
        Assert.assertEquals(0, projectRepository.findAll().size());
    }

    @Test
    public void existsById() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT1);
        Assert.assertTrue(projectRepository.existsById(USER1_PROJECT1.getId()));
        Assert.assertFalse(projectRepository.existsById(USER2_PROJECT1.getId()));
    }

    @Test
    public void existsByIdUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT1);
        Assert.assertTrue(projectRepository.existsByIdUserId(USER1.getId(), USER1_PROJECT1.getId()));
        Assert.assertFalse(projectRepository.existsByIdUserId(USER1.getId(), USER2_PROJECT1.getId()));
    }

    @Test
    public void findAll() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        for (@NotNull ProjectDTO project : USER1_PROJECT_LIST) {
            projectRepository.add(project);
        }
        Assert.assertEquals(USER1_PROJECT_LIST.size(), projectRepository.findAll().size());
    }

    @Test
    public void findAllUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        for (@NotNull ProjectDTO project : PROJECT_LIST) {
            projectRepository.add(project);
        }
        Assert.assertEquals(USER1_PROJECT_LIST.size(), projectRepository.findAllUserId(USER1.getId()).size());
    }

    @Test
    public void findOneById() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT1);
        projectRepository.add(USER2_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1.getId(), projectRepository.findOneById(USER1_PROJECT1.getId()).getId());
    }

    @Test
    public void findOneByIdUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT1);
        projectRepository.add(USER2_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1.getId(), projectRepository.findOneByIdUserId(USER1.getId(), USER1_PROJECT1.getId()).getId());
    }

    @Test
    public void getSize() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        for (@NotNull ProjectDTO project : PROJECT_LIST) {
            projectRepository.add(project);
        }
        Assert.assertEquals(PROJECT_LIST.size(), projectRepository.getSize());
    }

    @Test
    public void getSizeUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        for (@NotNull ProjectDTO project : PROJECT_LIST) {
            projectRepository.add(project);
        }
        Assert.assertEquals(USER1_PROJECT_LIST.size(), projectRepository.getSizeUserId(USER1.getId()));
        Assert.assertEquals(USER2_PROJECT_LIST.size(), projectRepository.getSizeUserId(USER2.getId()));
        Assert.assertEquals(ADMIN1_PROJECT_LIST.size(), projectRepository.getSizeUserId(ADMIN.getId()));
    }

    @Test
    public void remove() {
        Assert.assertNull(projectRepository.findOneById(USER1_PROJECT1.getId()));
        projectRepository.add(USER1_PROJECT1);
        Assert.assertNotNull(projectRepository.findOneById(USER1_PROJECT1.getId()));
        projectRepository.remove(USER1_PROJECT1);
        Assert.assertNull(projectRepository.findOneById(USER1_PROJECT1.getId()));
    }

    @Test
    public void removeUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT1);
        projectRepository.add(USER2_PROJECT1);
        projectRepository.removeUserId(USER1.getId(), USER1_PROJECT1);
        Assert.assertNull(projectRepository.findOneByIdUserId(USER1.getId(), USER1_PROJECT1.getId()));
    }

    @Test
    public void removeById() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT1);
        projectRepository.add(USER2_PROJECT1);
        projectRepository.removeById(USER1_PROJECT1.getId());
        entityManager.flush();
        entityManager.clear();
        Assert.assertNull(projectRepository.findOneById(USER1_PROJECT1.getId()));
    }

    @Test
    public void removeByIdUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT1);
        projectRepository.add(USER2_PROJECT1);
        projectRepository.removeByIdUserId(USER1.getId(), USER1_PROJECT1.getId());
        Assert.assertNull(projectRepository.findOneByIdUserId(USER1.getId(), USER1_PROJECT1.getId()));
    }

    @Test
    public void update() {
        @NotNull ProjectDTO project = new ProjectDTO("TEST", "TEST");
        project.setUserId(USER1.getId());
        projectRepository.add(project);
        Assert.assertEquals(project.getName(), projectRepository.findOneById(project.getId()).getName());
        project.setName("SUPER_PUPER_TEST");
        projectRepository.update(project);
        Assert.assertEquals(project.getName(), projectRepository.findOneById(project.getId()).getName());
    }

}
