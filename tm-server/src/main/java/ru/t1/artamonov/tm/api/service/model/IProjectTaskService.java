package ru.t1.artamonov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.repository.model.IProjectRepository;
import ru.t1.artamonov.tm.api.repository.model.ITaskRepository;

import javax.persistence.EntityManager;

public interface IProjectTaskService {

    @NotNull
    ITaskRepository getTaskRepository(@NotNull EntityManager entityManager);

    @NotNull
    IProjectRepository getProjectRepository(@NotNull EntityManager entityManager);

    void bindTaskToProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId);

    void removeProjectById(@Nullable final String userId, @Nullable final String projectId);

    void unbindTaskFromProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId);

}
