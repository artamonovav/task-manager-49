package ru.t1.artamonov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.artamonov.tm.dto.model.TaskDTO;
import ru.t1.artamonov.tm.enumerated.Sort;
import ru.t1.artamonov.tm.enumerated.Status;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ITaskDTOService {

    @NotNull
    ITaskDTORepository getRepository(@NotNull EntityManager entityManager);

    @NotNull
    TaskDTO add(@Nullable TaskDTO model);

    @NotNull
    TaskDTO add(@Nullable String userId, @Nullable TaskDTO model);

    @NotNull
    Collection<TaskDTO> add(@NotNull Collection<TaskDTO> models);

    @NotNull
    TaskDTO changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    void clear();

    void clear(@Nullable String userId);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    boolean existsById(@Nullable String id);

    boolean existsById(@Nullable String userId, @Nullable String id);

    @Nullable
    List<TaskDTO> findAll();

    @Nullable
    List<TaskDTO> findAll(@Nullable String userId);

    @Nullable
    List<TaskDTO> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable
    List<TaskDTO> findAll(@Nullable Comparator<TaskDTO> comparator);

    @Nullable
    List<TaskDTO> findAll(@Nullable String userId, @Nullable Comparator<TaskDTO> comparator);

    @Nullable
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    TaskDTO findOneById(@Nullable String id);

    @Nullable
    TaskDTO findOneById(@Nullable String userId, @Nullable String id);

    long getSize();

    long getSize(@Nullable String userId);

    @NotNull
    TaskDTO remove(@Nullable TaskDTO model);

    @NotNull
    TaskDTO remove(@Nullable String userId, @Nullable TaskDTO model);

    void removeAll(@Nullable Collection<TaskDTO> collection);

    @NotNull
    TaskDTO removeById(@Nullable String id);

    @NotNull
    TaskDTO removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    Collection<TaskDTO> set(@NotNull Collection<TaskDTO> models);

    @NotNull
    TaskDTO update(@NotNull TaskDTO model);

    @NotNull
    TaskDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

}
