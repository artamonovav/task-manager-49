package ru.t1.artamonov.tm.api.repository.dto;

import ru.t1.artamonov.tm.dto.model.ProjectDTO;


public interface IProjectDTORepository extends IUserOwnedDTORepository<ProjectDTO> {

}
