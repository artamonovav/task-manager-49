package ru.t1.artamonov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.artamonov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.artamonov.tm.comparator.CreatedComparator;
import ru.t1.artamonov.tm.comparator.StatusComparator;
import ru.t1.artamonov.tm.model.AbstractUserOwnedModel;
import ru.t1.artamonov.tm.model.User;

import javax.persistence.EntityManager;
import java.util.Comparator;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == StatusComparator.INSTANCE) return "status";
        else return "name";
    }

    public void add(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        model.setUser(entityManager.find(User.class, userId));
        entityManager.persist(model);
    }

    public void update(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        model.setUser(entityManager.find(User.class, userId));
        entityManager.merge(model);
    }

    public void removeUserId(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        if (existsByIdUserId(userId, model.getId())) entityManager.remove(model);
    }

}
