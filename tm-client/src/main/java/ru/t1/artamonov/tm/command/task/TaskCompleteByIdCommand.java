package ru.t1.artamonov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.dto.request.TaskChangeStatusByIdRequest;
import ru.t1.artamonov.tm.enumerated.Status;
import ru.t1.artamonov.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-complete-by-id";

    @NotNull
    private static final String DESCRIPTION = "Complete task by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(getToken());
        request.setTaskId(id);
        request.setStatus(Status.COMPLETED);
        getTaskEndpointClient().changeTaskStatusById(request);
    }

}
