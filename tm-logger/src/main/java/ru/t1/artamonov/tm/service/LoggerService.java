package ru.t1.artamonov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.LinkedHashMap;
import java.util.Map;

public final class LoggerService {

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @SneakyThrows
    public void log(final String text) {
        @NotNull final Map<String, Object> event = objectMapper.readValue(text, LinkedHashMap.class);
        @NotNull final String table = event.get("table").toString();
        final byte[] bytes = text.getBytes();
        @NotNull final File file = new File(table);
        if (!file.exists()) file.createNewFile();
        Files.write(Paths.get(table), bytes, StandardOpenOption.APPEND);
    }

}
